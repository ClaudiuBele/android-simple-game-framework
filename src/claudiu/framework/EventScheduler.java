package claudiu.framework;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map.Entry;

/**
 * Event Scheduler is an AbstractBehavior which lets you schedule an event that
 * will run after X seconds, with a possible initial event when first scheduling the event,
 *  The X seconds being either referred as real time or game time ( affected by
 *  {@link Game#timeScale} ).
 * <p>
 * When Scheduling an event, you will make an {@link AbstractEvent} that has a variable in
 * it in which we add {@link Game#delta} or {@link Game#rawDelta} with each behavior update.
 * <p>
 * After that variable is bigger than the time variable, it
 * runs the event that you passed as the first parameter, it removes the event from the list
 * 
 * @author Claudiu
 * 
 */
public class EventScheduler extends AbstractBehavior {

	// it is the other way around because you can change values but not keys
	// the Float is the timeRemaining, which will be changed with each update,
	// so we want it as the value
	
	/**
	 *  ArrayList in which we hold the AbstractEvents that themselves run the events.
	 */
	private ArrayList<AbstractEvent> events;
	
	public EventScheduler(AbstractObject obj) {
		super(obj,"EventScheduler");
		events = new ArrayList<AbstractEvent>();
	}
	
	@Override
	protected void run() {
		
		for (int i = 0; i < events.size(); i++) {
			events.get(i).run();
		}
	}
	
	public void scheduleEvent(final float seconds,final AbstractEvent event,final boolean truTime){
		events.add(new AbstractEvent() {

			private boolean trueTime = truTime;
			private float time = seconds;
			private float timeElapsed = 0;
			private AbstractEvent eventToRun = event;

			@Override
			public synchronized void run() {
				if (trueTime) {
					timeElapsed += rawDelta;
				} else {
					timeElapsed += delta;
				}
				if (timeElapsed > time) {
					eventToRun.run();
					events.remove(events.indexOf(this));
				}

			}
		});	}
	public void scheduleEvent(float seconds, AbstractEvent event, AbstractEvent firstEvent, boolean truTime){
		firstEvent.run();
		scheduleEvent(seconds, event, truTime);
	}
	public void removeEvent(AbstractEvent event){
		events.add(event);
	}
}
