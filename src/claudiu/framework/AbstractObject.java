package claudiu.framework;

import java.util.ArrayList;
import java.util.HashMap;

import android.util.Log;

public abstract class AbstractObject {

	
	
	
	
	/**
	 * Assigned true if it is a child of a parent
	 */
	protected boolean isChild;
	
	/**
	 * Assigned true if it has at least one child
	 */
	protected boolean isParent;
	
	/**
	 *  List of children. When adding a new Children, we add them to the list
	 */
	protected ArrayList<AbstractObject> children;
	
	/**
	 * The parent of a child. Assigned in {@link #addChild(AbstractObject)}
	 */
	protected AbstractObject parent;
	
	
	/**
	 * The time in seconds since last update. Multiplied by {@link Game#timeScale}
	 */
	public float delta;
	/**
	 * The time in seconds since last update.
	 */
	public float rawDelta;
	
	/**
	 * Assigned in the class, to be able to react to some events depending on an
	 * object's type
	 */
	protected String type;

	/**
	 * Assigned in the constructor, used to be able to take out anAbstractObject
	 * out of the map by name
	 */
	protected String name;

	/**
	 * Used to be able to access the object from an annonymous inner type
	 */
	protected AbstractObject instance;

	/**
	 * The view in which the object is
	 */
	protected AbstractView view;
	
	/**
	 * Map that helps us get a behavior based on it's name.
	 */
	protected HashMap<String, AbstractBehavior> behaviors;

	/**
	 * The position of the object
	 */
	protected Point position;
	/**
	 *  The local position of the object. It is used if the object is a child.
	 *  <p>
	 *  This acts as an offset to the parent's position, the child's position being
	 *  made by adding it's own {@link #localPosition} and the parent's {@link #localPosition}.
	 */
	protected Point localPosition;
	
	/**
	 * The level at which to render, 0 for farthest away from the screen, more then that for each layer.
	 */
	protected int level;
	
	/**
	 * The level ( level of the parent + this) at which to render, 0 for farthest away from the screen, more then that for each layer.
	 */
	protected int localLevel;
	
	/**
	 * Boolean that decides whether or not we invoke the 3 run methods or not.
	 * By default, isEnabled is true.
	 * <p>
	 * Can be set to false by calling pause();
	 * <p>
	 * Can be set to true by calling resume();
	 * 
	 */
	protected boolean isRunning;
	
	
	
	
	
	
	
	/**
	 * The constructor for the AbstractObject. Adds the object to the parameter
	 * view, in the table with the type and name keys
	 * <p>
	 * DO NOT ADD CODE IN A SUBCLASSES CONSTRUCTOR
	 * <p>
	 * Adding behaviors to the object will be done in the create method
	 * 
	 * @param context
	 */
	public AbstractObject(String name, AbstractView view) {

		this.type 		= "Default";
		this.name 		= "Default";
		
		this.isChild    = false;
		this.isParent   = false;
		this.parent     = null;
		
		this.isRunning  = true;
		
		
		this.position 	= new Point(0, 0);
		this.localPosition =  new Point(0, 0);
		
		this.behaviors 	= new HashMap<String, AbstractBehavior>();
		this.children   = new ArrayList<AbstractObject>();
		this.instance 	= this;
		this.view 		= view;
		this.name       = name;
		this.level 		= 0;

		createObject();
		
		
		// add the object to the list, map
		view.add(instance);
	}

	/**
	 * Put all AbstractBehaviors and additional logic here
	 * 
	 */
	public abstract void createObject();

	/**
	 * This method is used to add logic that is specific to the object, instead
	 * of working around with another behavior.
	 */
	public abstract void objectSpecificUpdate();

	/**
	 * The update function that will be called in the view.
	 * <p>
	 * Calls the update in
	 * every behavior of the object, which in turn updates the
	 * {@link AbstractBehavior}'s delta, and calling
	 * <p>
	 * {@link AbstractBehavior.runAtStartOfUpdate} ,
	 * {@link AbstractBehavior.run} and
	 * {@link AbstractBehavior.runAtEndOfUpdate}, in this order.
	 * <p>
	 * After all the behaviors have been updated, we call the
	 * {@link AbstractObject.objectSpecificUpdate}
	 * 
	 * @param delta
	 */
	public final void update() {
		this.delta 		= Game.delta;
		this.rawDelta 	= Game.rawDelta;
		
		// the Object is stopped, won't run
		if(!this.isRunning) return;
		
		// The Parent is stopped, won't run
		// short-circuit evaluation y'all
		if(this.parent !=null && !this.parent.isRunning) return;
		
		if(isChild){
			position.x = this.localPosition.x + parent.position.x;
			position.y = this.localPosition.y + parent.position.y;
			level = localLevel + parent.level;
		}
		
		for (AbstractBehavior behavior : behaviors.values()) {
			behavior.update();
		}
		objectSpecificUpdate();
	}

	public final void addChild(AbstractObject obj){
		this.children.add(obj);
		this.isParent = true;
		Log.v("Game", "added children "+obj.getName()+" ,start of addChild");
		obj.isChild = true;
		obj.parent = this;
		obj.position.setXandY(
				this.position.x + obj.localPosition.x,
				this.position.y + obj.localPosition.y);
		
		// update child's level
		obj.level = obj.localLevel + this.level;
		Log.v("Game", "added children "+obj.getName()+" ,end of addChild");

	}
	/** Method used for setting up the local position of a child.
	 * <p>
	 * {@link #localPosition} is afterwards used in {@link #update()}, where
	 * you add it to the parent's position to make up the child's {@link #position}
	 * @param localLevel
	 */
	public final void setLocalPosition(float x, float y){
		if(parent!=null){
			this.localPosition.x = x;
			this.localPosition.y = y;
		}
	}
	/** Method used for setting up the local level of a child.
	 * <p>
	 * {@link #localLevel} is afterwards used in {@link #update()}, where
	 * you add it to the parent's level to make up the child's {@link #level}
	 * @param localLevel
	 */
	public final void setLocalLevel(int localLevel){
		if(parent!=null){
			this.localLevel = localLevel;
		}
	}
	
	
	
	
	
	
	
	// A little more special setters and getters
	public AbstractBehavior getBehavior(String name) {
		if (behaviors.containsKey(name))
			return behaviors.get(name);
		return null;
	}
	public final void pause() {
		this.isRunning = false;
	}
	public final void resume() {
		this.isRunning = true;
	}
	
	// Default Setters and getters
	public HashMap<String, AbstractBehavior> getBehaviors() {
		return behaviors;
	}
	public final String getType() {
		return type;
	}
	public final String getName() {
		return name;
	}
	public final Point getPosition() {
		return position;
	}
	public final void setPosition(Point position) {
		this.position = position;
	}


	
}
