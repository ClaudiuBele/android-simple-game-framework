package claudiu.framework;

import android.graphics.Canvas;
import android.graphics.Paint;

public class FontDrawer extends AbstractBehavior {

	/**
	 * How many pixels on X and Y axis the image has to be pushed away from the
	 * upper-left corner of the object's position
	 */
	private int offsetX, offsetY;
	public int posX, posY;
	public boolean isFixed;

	private String text;

	private Paint p;

	public FontDrawer(AbstractObject obj) {
		super(obj, "FontDrawer");
		this.isFixed = true;
		this.offsetX = this.offsetY = 0;
		this.posX = 0;
		this.posY = 0;
		this.p = new Paint(Paint.ANTI_ALIAS_FLAG);
		this.p.setARGB(255, 255, 255, 255);
		this.p.setAntiAlias(true);
		this.p.setTextSize(20);
		this.text = "DEFAULT";
	}

	@Override
	protected void run() {
		
		if(isFixed)
		{}
		else
		{
			this.posX = (int)this.object.position.x + this.offsetX;
			this.posY = (int)this.object.position.y + this.offsetY;
		}
	}

	public void draw(Canvas canvas) {
		canvas.drawText(this.text, posX, posY, this.p);
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getText() {
		return text;
	}
	public void setOffset(int offsetX, int offsetY) {
		this.offsetX = offsetX;
		this.offsetY = offsetY;
	}
	public void setPosition(int posX, int posY)
	{
		this.posY = posY;
		this.posX = posX;
	}
	public void setColor(int a, int r, int g, int b){
		p.setARGB(a, r, g, b);
	}
	public void setFontSize(int fontSize){
		p.setTextSize(fontSize);
	}
}
