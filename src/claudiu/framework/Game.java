package claudiu.framework;

import claudiu.framework.AbstractView.FrameUpdaterTask;
import android.R;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;

public class Game {

	/**
	 * Used so we don't initialise the values twice, becomes true when we call
	 * initialise, which is exitted from the start if the value is true
	 * 
	 * @see Game#initialise(Context)
	 */
	public static boolean isInitialised = false;

	/**
	 * The context's resources, assigned in {@link Game#initialise(Context)}
	 */
	public static Resources res;

	/**
	 * The game's context, assigned in {@link Game#initialise(Context)}
	 */
	public static Context context;

	/**
	 * The canvas on which we draw, assigned in
	 * {@link AbstractView#onDraw(Canvas)}
	 */
	public static Canvas canvas;

	/**
	 * Draws green squares for each collider rectangle
	 */
	public static Drawable debugCollider;

	/**
	 * The values on the X and Y axis of a touch event ( pressing the screen ).
	 * Assigned in {@link AbstractView#onTouchEvent(android.view.MotionEvent)}
	 */
	public static int inputX, inputY;
	/**
	 * The values of X and Y of a touch event ( pressing the screen )
	 * encapsulated in a {@link Point} object. Assigned in
	 * {@link AbstractView#onTouchEvent(android.view.MotionEvent)}
	 */
	public static Point input;

	/**
	 *  The width and the height of the screen.
	 *  <p>
	 *  Assigned in {@link Game#initialise(Activity)}
	 */
	public static int width, height;
	/**
	 * 	The width and the height of the screen in a Point object
	 * <p>
	 * Assigned in {@link Game#initialise(Activity)}
	 */
	public static Point screenSize;
	
	/** Accelerometer x,y,z axis values
	 * <p>
	 * Assigned in {@link Game#initialise(Activity)}
	 */
	public static Point3D acceleration = new Point3D();
	
	/**
	 * Time in seconds since last frame update. Assigned in
	 * {@link FrameUpdaterTask#run()}
	 */
	public static float delta;
	
	/**
	 * Real time in seconds since last frame update. Assigned in {@link FrameUpdaterTask#run()} .
	 * <p>
	 * It is used when you need a delta in {@link EventScheduler } and {@link RecurringEventScheduler}, and the delta needs to represent real time, not sped up/ slowed Down 
	 */
	public static float rawDelta;

	/**
	 * Sets the speed of the game, multiplying the {@link Game#delta} by it.
	 * <p>
	 * <b>Normal</b> game speed is at timeScale = 1, the game is <b>paused</b> at timeScale = 0 , the game has <b>double</b> it's speed at timeScale = 2;
	 * <p>
	 * Any float value is valid, but if it is negative, it will become 0.
	 * <p>
	 * You have to set it in {@link AbstractView#createView()}, and with every update, the
	 * timeScale is changed if negative, to 0, in {@link FrameUpdaterTask#run()}
	 * , before multiplying delta by it.
	 */
	public static float timeScale = 1;

	/** The number of times in a second the objects will be updated
	 * <p>
	 * Should be changed to {@link AbstractView#createView() }
	 * 
	 */
	public static int framesPerSecond = 30;
	
	/**
	 * Initializes key variables, called in
	 * {@link AbstractView#AbstractView(Context)} constructor
	 * 
	 * @param context
	 */
	public static void initialise(Activity activity) {
		if (isInitialised)
			return;
		
		DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        width = metrics.widthPixels;
        height = metrics.heightPixels;
		screenSize = new Point(width, height);
		
		
		Game.context = activity;
		Game.res = Game.context.getResources();
		Game.timeScale = 1;
		Game.screenSize = new Point();
		Game.input = new Point();

	}
}
