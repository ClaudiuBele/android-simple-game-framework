package claudiu.framework;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

/**
 * All the views will extends this, and the only thing that you have to do in
 * this class is add the objects to the view.
 * 
 * 
 * @author Claudiu Bele
 * 
 */
public abstract class AbstractView extends View implements SensorEventListener {

	/**
	 * The Activity class that we will use in the intent, in order to change activities.
	 * <p>
	 * Used for changing the activity in {@link FrameUpdaterTask#run()}, called when {@link #nextActivity} is not null and 
	 * there are no other objects in the {@link #objectList}.
	 * <p>
	 * Changed in {@link #resetView()} to the current View's Activity,
	 *  or in {@link #changeActivity(Class)} with the parameter passed.
	 */
	private Class nextActivity = null;
	
	/**
	 * The Activity's context, assigned in {@link #AbstractView(Activity)}, assigning the context
	 * as the view passed as a parameter.
	 * <p>
	 * Used when making an {@link Intent}, in order to change the Activity, in {@link FrameUpdaterTask#run()}.
	 */
	private Context myContext;
	
	
	/**
	 * Used for setting up sensors and to register them to the listener
	 * <p>
	 * This is very important as that enables calls to
	 * {@link #onSensorChanged(SensorEvent)}
	 * <p>
	 * Assigned in constructor, no need to worry about it.
	 * 
	 */
	private SensorManager sensorManager;

	/**
	 * Sensor in which we will asign the Accelerometer, and which will be used
	 * to register a listener.
	 * <p>
	 * Assigned in {@link #AbstractView(Activity)}, by getting the default
	 * Accelerometer sensor from {@link #sensorManager}
	 * 
	 */
	private Sensor sensor;

	// variables that help the game run the same regardless of frame rate
	/**
	 * The time in seconds since last update
	 * 
	 */
	public static float delta;
	/**
	 * The time of the last update
	 * 
	 */
	public static long timeLastFrame;

	/**
	 * List in which we will put objects that we want to remove, removing them
	 * at the end of the frame.
	 * <p>
	 * Iterated over in FrameUpdaterTask.run(), at the end of the frame it will
	 * be empty
	 */
	protected ArrayList<AbstractObject> toRemove;

	/**
	 * List in which we will put objects that we want to add, adding them at the
	 * end of the frame.
	 * <p>
	 * Iterated over in FrameUpdaterTask.run(), at the end of the frame it will
	 * be empty
	 */
	protected ArrayList<AbstractObject> toAdd;

	/**
	 * List of Objects that we will go through and run through, calling the
	 * update function of every updateableBehavior
	 * <p>
	 * Iterated over in FrameUpdaterTask.run()
	 */
	protected ArrayList<AbstractObject> objectList;
	protected HashMap<String, HashMap<String, AbstractObject>> objectsMap;

	/**
	 * AbstractView that references the object built, to be able to retrieve the
	 * view from inside the Frame Updater Task
	 */
	protected AbstractView instance;

	/**
	 * The objectsList will be compared using this Comparator to draw stuff
	 * based on their layer and y position.
	 * <p>
	 * Used in Frame Update Task's run method, on objectList.
	 */
	protected Comparator<AbstractObject> gameComparator;

	/**
	 * The constructor for the AbstractView. Sets up the context and initializes
	 * objects
	 * <p>
	 * DO NOT ADD CODE IN A SUBCLASSES CONSTRUCTOR
	 * <p>
	 * Adding objects to the view will be done in the create method
	 * 
	 * @param context
	 */
	public AbstractView(Activity activity) {

		super(activity);
		Game.initialise(activity);

		this.myContext = activity;
		
		this.toRemove = new ArrayList<AbstractObject>();
		this.toRemove.clear();

		this.toAdd = new ArrayList<AbstractObject>();
		this.toAdd.clear();

		this.objectList = new ArrayList<AbstractObject>();
		this.objectList.clear();

		this.objectsMap = new HashMap<String, HashMap<String, AbstractObject>>();
		this.objectsMap.clear();

		this.instance = this;

		sensorManager = (SensorManager) activity
				.getSystemService(Context.SENSOR_SERVICE);
		sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		sensorManager.registerListener(this, sensor,
				SensorManager.SENSOR_DELAY_GAME);

		setKeepScreenOn(true); // no screen dim
		activity.requestWindowFeature(Window.FEATURE_NO_TITLE); // no title line
		activity.getWindow().setFlags(
				WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN); // full screen

		// will first draw the stuff on the lowest layer
		this.gameComparator = new Comparator<AbstractObject>() {

			@Override
			public int compare(AbstractObject first, AbstractObject second) {
				return (first.level * 10000 + (int) first.position.y)
						- (second.level * 10000 + (int) second.position.y);
			}
		};

		createView();

		Timer timer = new Timer();
		FrameUpdaterTask mainTask = new FrameUpdaterTask();
		timer.scheduleAtFixedRate(mainTask, 0, 1000 / Game.framesPerSecond);

		//
	}

	/**
	 * A second method that comes with {@link SensorEventListener }
	 */
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	/**
	 * Handles changes in sensors that we registered ( in
	 * {@link AbstractView#AbstractView(Activity)} ), in variable
	 * {@link AbstractView#sensorManager}
	 * <p>
	 * Currently it handles accelerometer data, assigning it it
	 * {@link Game#acceleration}
	 * <p>
	 * This method is overwritten thanks to implementing the
	 * {@link SensorEventListener}
	 * 
	 * @param event
	 *            one event that was registered
	 * @see #sensorManager
	 * @see #sensor
	 * @see #objectList
	 * @see ReactToAccelerometer
	 * @see ReactToAccelerometer#feedData(Point3D)
	 */
	@Override
	public final void onSensorChanged(SensorEvent event) {
		try {

			if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

				Game.acceleration.x = event.values[0] / 10;
				Game.acceleration.y = event.values[1] / 10;
				Game.acceleration.z = event.values[2] / 10;

				for (int i = 0; i < objectList.size(); i++) {
					if (objectList.get(i).getBehavior("ReactToAccelerometer") != null) {
						((ReactToAccelerometer) objectList.get(i).getBehavior(
								"ReactToAccelerometer"))
								.feedData(Game.acceleration);

					}
				}
			}
		} catch (Exception e) {
			Log.v("Game", "Got an exception from OnSensorChanged");
		}

	}

	/**
	 * Method that handles drawing objects on the screen.
	 * <p>
	 * Sets {@link Game#canvas} to the canvas passed as a parameter.
	 * <p>
	 * Goes over all the objects, and if they have a {@link Renderable}
	 * Behavior, it calls {@link Renderable#draw(Canvas)} on that Renderable
	 * object
	 * 
	 * @param canvas
	 *            The canvas on which we draw
	 * @see Renderable
	 * @see Renderable#draw(Canvas)
	 * @see Game.canvas
	 * @see Canvas
	 */
	@Override
	public final void onDraw(Canvas canvas) {
		Game.canvas = canvas;

		for (int i = 0; i < objectList.size(); i++) {
			if (objectList.get(i) == null)
				continue;

			try {

				if (objectList.get(i).getBehavior("Renderable") != null) {
					((Renderable) objectList.get(i).getBehavior("Renderable"))
							.draw(canvas);
				}
				if (objectList.get(i).getBehavior("FontDrawer") != null) {
					((FontDrawer) objectList.get(i).getBehavior("FontDrawer"))
							.draw(canvas);
				}
			} catch (Exception e) {
				Log.v("Game", "Caught an exception :( don't know why yet.");
			}
		}

	}

	/**
	 * Method that handles touch interactions. Currently, it handles
	 * <p>
	 * {@link MotionEvent#ACTION_DOWN} , {@link MotionEvent#ACTION_UP} and
	 * {@link MotionEvent#ACTION_MOVE}.
	 * <p>
	 * Based on the event, it runs( if available ) the corresponding
	 * AbstractEvent found in ReactToTouch Method
	 * 
	 */
	@Override
	public synchronized boolean onTouchEvent(MotionEvent event) {
		// Log.v("Game","In onTouchEvent");

		int eventId = event.getAction();

		Game.inputX = (int) event.getX();
		Game.inputY = (int) event.getY();
		Game.input.x = event.getX();
		Game.input.y = event.getY();

		switch (eventId) {

		case MotionEvent.ACTION_DOWN:
			for (int i = 0; i < objectList.size(); i++) {
				if (objectList.get(i).getBehavior("ReactToTouch") != null) {
					if (!((ReactToTouch) objectList.get(i).getBehavior(
							"ReactToTouch")).doesPointClick(Game.input))
						continue;
					((ReactToTouch) objectList.get(i).getBehavior(
							"ReactToTouch")).getDownEvent().run();
				}
			}
			break;

		case MotionEvent.ACTION_UP:
			for (int i = 0; i < objectList.size(); i++) {
				if (objectList.get(i).getBehavior("ReactToTouch") != null) {
					if (!((ReactToTouch) objectList.get(i).getBehavior(
							"ReactToTouch")).doesPointClick(Game.input))
						continue;

					((ReactToTouch) objectList.get(i).getBehavior(
							"ReactToTouch")).getUpEvent().run();
				}
			}

			break;

		case MotionEvent.ACTION_MOVE:
			for (int i = 0; i < objectList.size(); i++) {
				if (objectList.get(i).getBehavior("ReactToTouch") != null) {
					if (!((ReactToTouch) objectList.get(i).getBehavior(
							"ReactToTouch")).doesPointClick(Game.input))
						continue;

					((ReactToTouch) objectList.get(i).getBehavior(
							"ReactToTouch")).getMoveEvent().run();
				}
			}

			break;
		}

		return true;
	}

	/**
	 * Method in which we have to set up all the objects that we want to add to
	 * the scene
	 * <p>
	 * Has to be overriden in the subclass
	 */
	public abstract void createView();

	/**
	 * Puts an objects in a queue, to be removed at the end of the frame In
	 * FrameUpdaterTask's run method {@link FrameUpdaterTask.run}
	 * 
	 * @param objectToRemove
	 */
	public final void remove(AbstractObject objectToRemove) {
		toRemove.add(objectToRemove);
		if (objectToRemove.isParent) {
			for (int i = 0; i < objectToRemove.children.size(); i++) {
				remove(objectToRemove.children.get(i));
			}
		}
	}

	/**
	 * Puts an objects in a queue, to be added at the end of the frame In
	 * FrameUpdaterTask's run method {@link FrameUpdaterTask.run}
	 * 
	 * @param objectToRemove
	 */
	public final void add(AbstractObject objectToAdd) {
		toAdd.add(objectToAdd);
	}

	/**
	 * Pauses all objects currently in the view, and adds them in the queue for
	 * removal at the end of frame update
	 */
	public final void removeAll() {
		for (int i = 0; i < objectList.size(); i++) {
			objectList.get(i).pause();
			remove(objectList.get(i));
		}
	}

	/**
	 *  Resets the current view.
	 *  <p>
	 *  Calls {@link #changeActivity(Class)} with the parameter being <b>this.getClass()</b>, which clears {@link #objectList} and {@link #objectsMap},
	 *   and then changes the desired activity in {@link #nextActivity}.
	 */
	public final void resetView(){
		changeActivity(((Activity)(myContext)).getClass());
	}
	
	/**
	 *  Method in which we pass the desired {@link Activity} that we want to change to.
	 *  <p>
	 *  Clears the objects from the current view, setting the {@link #nextActivity} to the one passed as a parameter.
	 *  <p>
	 *  It is called in {@link #resetView()}, which passes <b>this.getClass()</b> as the parameter.
	 * @param selectedActivity
	 */
	public final void changeActivity(Class selectedActivity)
	{
		removeAll();
		Game.timeScale = 1;
		nextActivity = selectedActivity;
	}
	
	final class FrameUpdaterTask extends TimerTask {

		public FrameUpdaterTask() {
			timeLastFrame = System.currentTimeMillis();
		}

		/**
		 * Method called 30 times in a second, which updates all the objects and
		 * does other maintenance stuff.
		 * <p>
		 * Updates {@link AbstractView#delta}, that shows exactly how much time
		 * in seconds it took since the last call
		 * 
		 * 
		 */
		@Override
		public synchronized void run() {

			// 0 -> pause
			// 0.5 -> 1/2 speed
			// 2 -> 2 * speed
			// If timeScale is negative, it becomes 0
			Game.timeScale = Math.max(Game.timeScale, 0);

			// update raw delta to be time between frames
			Game.rawDelta = (System.currentTimeMillis() - timeLastFrame) / 1000f;

			// 60
			// update delta to be rawDelta multiplied by gameSpeed
			Game.delta = Game.rawDelta * Game.timeScale;

			// variables that we will subtract the current time from next frame
			// to determine the time between frames
			timeLastFrame = System.currentTimeMillis();

			try {
				Collections.sort(objectList, gameComparator);
			} catch (Exception e) {
			}
			updateObjects();
			removeObjects();
			addObjects();

			instance.postInvalidate();
			if(instance.nextActivity !=null && objectList.size()==0){
				Intent gameIntent =  new Intent(myContext, nextActivity);
				myContext.startActivity(gameIntent);
				nextActivity = null;
				cancel();
			}
		}

		/**
		 * Goes over the update method in every object that has been assigned to
		 * the view
		 * 
		 */
		private void updateObjects() {
			for (int i = 0; i < objectList.size(); i++) {
				objectList.get(i).update();
			}
		}

		/**
		 * Remove objects currently in a queue to be removed
		 */
		private void removeObjects() {
			// handles removing objects that were to be removed during this
			// frame
			for (int i = 0; i < toRemove.size(); i++) {

				// Checks whether or not the object we want to remove exists
				if (objectsMap.containsKey(toRemove.get(i).type)) {

					if (objectsMap.get(toRemove.get(i).type).containsKey(
							toRemove.get(i).name)) {
						objectsMap.get(toRemove.get(i).type).remove(
								toRemove.get(i).name);
						
						// remove Hashmap if it's empty
						if(objectsMap.get(toRemove.get(i).type).size() == 0){
							objectsMap.remove(toRemove.get(i).type);
						}
						objectList.remove(toRemove.get(i));
					}
				}

				// remove the object from the list
			}
			toRemove.clear();
		}

		/**
		 * Adds objects currently in a queue to be added
		 */
		private void addObjects() {

			for (int i = 0; i < toAdd.size(); i++) {
				objectList.add(toAdd.get(i));
				if (!objectsMap.containsKey(toAdd.get(i).type)) {
					objectsMap.put(toAdd.get(i).type,
							new HashMap<String, AbstractObject>());
				}
				objectsMap.get(toAdd.get(i).type).put(toAdd.get(i).name,
						toAdd.get(i));
			}
			toAdd.clear();

		}

	}

	// Default Setters and getters
	public ArrayList<AbstractObject> getObjectList() {
		return objectList;
	}

	public HashMap<String, HashMap<String, AbstractObject>> getObjectsMap() {
		return objectsMap;
	}

}
