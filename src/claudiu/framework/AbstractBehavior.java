package claudiu.framework;

/**
 * Class that will handle a single behavior
 * 
 * 
 * @author Claudiu Bele
 * 
 */
public abstract class AbstractBehavior {

	/**
	 * The time in seconds since last update
	 * 
	 */
	protected float delta;

	protected float rawDelta;
	
	/**
	 * String that uniquely identifies a specific Behavior among others. Used
	 * for retrieving an object's specific behavior
	 * 
	 */
	protected String name;

	/**
	 * Boolean that decides whether or not we invoke the 3 run methods or not.
	 * By default, isEnabled is true.
	 * <p>
	 * Can be set to false by calling pause();
	 * <p>
	 * Can be set to true by calling resume();
	 * 
	 */
	protected boolean isRunning;

	/**
	 * AbstractBehavior used to be able to reference the behavior we are in, while writing
	 * code in an anonymous inner class ( AbstractEvent most likely, in which
	 * calling "this" refers to the AbstractEvent, not AbstractBehavior)
	 */
	protected AbstractBehavior instance;

	/**
	 *  The AbstractObject that owns the behavior.
	 */
	protected AbstractObject object;
	
	
	/**
	 * The view in which the object owning the behavior is
	 */
	protected AbstractView view;
	
	/**
	 *  Event that if assigned, will be ran in {@link #runAtStartOfUpdate()}
	 */
	protected AbstractEvent startOfUpdateEvent;
	
	/**
	 *  Event that if assigned, will be ran in {@link #runAtEndOfUpdate()}
	 */
	protected AbstractEvent endOfUpdateEvent;
	
	
	
	
	
	public AbstractBehavior(AbstractObject obj, String name) {
		this.isRunning = true;
		this.name = name;
		this.instance = this;
		this.object = obj;
		this.view = obj.view;
		obj.getBehaviors().put(this.name, this);
	}

	/**
	 * Method that is called with every frame update, changing the local delta
	 * value and calling the 3 run methods. This method is called when iterating
	 * over all of an object's behaviors, in {@link AbstractObject.update},
	 * which in turn is called in the AbstractView
	 * <p>
	 * Don't override this.
	 */
	protected final void update() {
		this.delta 		= Game.delta;
		this.rawDelta 	= Game.rawDelta;
		
		// the Behavior is stopped, won't run
		if (!isRunning) return;
			
			
		runAtStartOfUpdate();
		run();
		runAtEndOfUpdate();
		
	}

	/**
	 * Method that should prepare before any big major changes in data that
	 * happens in the run method
	 * <p>
	 * Method is called in {@link AbstractBehavior.update}
	 */
	protected  void runAtStartOfUpdate(){
		if(startOfUpdateEvent !=null){
			startOfUpdateEvent.run();
		}
	}

	/**
	 * Method that should process all the data that has been changed in the run
	 * method
	 * <p>
	 * Method is called in {@link AbstractBehavior.update}
	 */
	protected  void runAtEndOfUpdate(){
		if(endOfUpdateEvent != null){
			endOfUpdateEvent.run();
		}
	}

	/**
	 * Method in which we will add all the core functionality of a behavior
	 */
	protected abstract void run();

	
	
	
	
	
	
	public final void pause() {
		this.isRunning = false;
	}
	public final void resume() {
		this.isRunning = true;
	}

	
	
	
	
	
	// Default Setters and getters
	public boolean isEnabled() {
		return isRunning;
	}
	public void setStartOfUpdateEvent(AbstractEvent startOfUpdateEvent) {
		this.startOfUpdateEvent = startOfUpdateEvent;
	}
	public void setEndOfUpdateEvent(AbstractEvent endOfUpdateEvent) {
		this.endOfUpdateEvent = endOfUpdateEvent;
	}
	
	public void remove(){
		object.getBehaviors().remove(this.name);
	}
}
