package claudiu.framework;

public class Point{
	
	protected float x, y;

	public Point(float f, float g){
		this.x = f;
		this.y = g;
	}
	public Point(){
		this.x = 0;
		this.y = 0;
	}
	
	public float getX() {
		return x;
	}
	public void setX(float x) {
		this.x = x;
	}
	public float getY() {
		return y;
	}
	public void setY(float y) {
		this.y = y;
	}
	public void setXandY(float x, float y){
		this.x = x;
		this.y = y;
	}
}