package claudiu.framework;

import java.util.ArrayList;
import java.util.HashMap;

import android.R;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.Log;

/**
 * Behavior that handles drawing an image on the screen
 * <p>
 * The location at which it will draw is either having
 * {@link AbstractObject#position } be the center or the image , or having
 * {@link AbstractObject#position } the upper-left corner of the image
 * <p>
 * The only mandatory setters to call are one of the setters that changes the
 * {@link Drawable}, and one of the setters that changes the width and height, and the one that changes the offsetX and offsetY
 * <p>
 * The setters that change the <b>Drawable </b>are :
 * {@link Renderable#setImage(Drawable)} , {@link Renderable#setImage(int)}
 * <p>
 * The setters that change the <b>Width & Height</b> are :
 * {@link Renderable#setSize(int)} , {@link Renderable#setSize(int, int)}
 * <p>
 * The setter that changes the <b>Offsets</b> is : {@link Renderable#setOffsets(int, int)}
 * <p>
 * Additional setter is for changing where to draw the image, related to the
 * object's position , which changes based on
 * {@link Renderable#setPointCenter(boolean)}
 * 
 * @see AbstractObject#position
 * @see AbstractView#onDraw(Canvas)
 * @see Drawable
 * @author Claudiu Bele
 * 
 */
public class Renderable extends AbstractBehavior {


	public HashMap<String,Image> imagesToDraw;
	
	
	
	public Renderable(AbstractObject obj) {
		super(obj, "Renderable");
		
		this.imagesToDraw = new HashMap<String, Renderable.Image>();
	}

	/**
	 * Updates the bounds of the Image to match up with a possibly changed
	 * object position.
	 * <p>
	 * The bounds are made based on the {@link AbstractObject#position} ,
	 * {@link Renderable#width} , {@link Renderable#height} and
	 * {@link Renderable#isPointCenter}
	 * <p>
	 * If isPointCenter is true , the position Point of the object will be in
	 * the middle of the image, otherwise it will be in the upper-left corner
	 * 
	 * @see AbstractObject#position
	 * @see Point
	 * @see Renderable#height
	 * @see Renderable#width
	 * @see Renderable#isPointCenter
	 */
	@Override
	protected void run() {
		
		for(Image currImage : imagesToDraw.values()){
			currImage.image.setBounds(
				(int)(object.position.x + currImage.offsetX),
				(int)(object.position.y + currImage.offsetY),
				(int)(object.position.x + currImage.width  + currImage.offsetX),
				(int)(object.position.y + currImage.height + currImage.offsetY));
		}

	}

	/**
	 * Draws the Drawable on the canvas passed as a parameter
	 * <p>
	 * This method is called in the {@link AbstractView#onDraw(Canvas)} method,
	 * calling this method in each {@link AbstractObject} present in the
	 * {@link AbstractView#objectList} , if they have a Renderable behavior
	 * 
	 * @see Drawable
	 * @see Canvas
	 * @see AbstractView#onDraw(Canvas)
	 * @param canvas
	 */
	public void draw(Canvas canvas) {
//		image.draw(canvas);
		
		for(Image currImage :imagesToDraw.values()){
			if(currImage.enabled){
				currImage.image.draw(canvas);
			}
		}
	}

	// Custom Setters and Getters
	
	public void addImage(int RDrawableId, String name){
		Image newImage = new Image();
		newImage.setImage(RDrawableId);
		imagesToDraw.put(name, newImage);
	}
	public Image getImage(String name){
		return imagesToDraw.get(name);
	}
	public class Image{
		public Drawable image;
		public float offsetX, offsetY,width,height;
		public boolean enabled;
		public Image() {
			this.enabled = true;
			this.width = this.height = 50;
		}
		/** Sets the image to be drawn having the middle the object's position
		 * PRECONDITION : Set Size of Image
		 * 
		 */
		public void setPositionToBeCenter() {
			this.offsetX = -this.width/2;
			this.offsetY = -this.height/2;
		}
		public void setImage(int RDrawableId) {
			this.image = Game.res.getDrawable(RDrawableId);
		}

		public void setSize(float width, float height) {
			this.height = height;
			this.width = width;
		}
		public void setSize(float size) {
			this.width = size;
			this.height = size;
		}
		public void setOffsets(int offsetX, int offsetY){
			this.offsetX = offsetX;
			this.offsetY = offsetY;
		}
		
		// Default Setters and getters
		public Drawable getImage() {
			return image;
		}
		public void setImage(Drawable image) {
			this.image = image;
		}
		public float getWidth() {
			return width;
		}
		public float getHeight() {
			return height;
		}
		public float getOffsetX() {
			return offsetX;
		}
		public float getOffsetY() {
			return offsetY;
		}
		/** Sets the transparency of the image, values between 0 and 255
		 * <p>
		 * 0 for not visible, 255 for solid.
		 * 
		 * @param alpha
		 */
		public void setAlpha(int alpha){
			image.setAlpha(alpha);
		}
		
	}
	
	

	
	
	
}
