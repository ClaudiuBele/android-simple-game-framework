package claudiu.framework;

import java.util.ArrayList;

import android.util.Log;

/**
 * Recurring Event Scheduler is an AbstractBehavior which lets you schedule an event that
 * will run every X seconds, for Y amount of times each, The X seconds being
 * either referred as real time or game time ( affected by {@link Game#timeScale} ).
 * <p>
 * When Scheduling an event, you will make an {@link AbstractEvent} that has a variable in
 * it in which we add {@link Game#delta} or {@link Game#rawDelta} with each behavior update.
 * <p>
 * After that variable is bigger than the frequency variable, it
 * runs the event that you passed as the first parameter, that variable becoming
 * 0 again, decreasing times by one, if times > 0, it starts adding up deltas again. add event to {@link RecurringEventScheduler#events}
 * 
 * @author Claudiu
 * 
 */
public class RecurringEventScheduler extends AbstractBehavior {

	/**
	 *  ArrayList in which we hold the AbstractEvents that themselves run the events.
	 */
	private ArrayList<AbstractEvent> events;

	protected float overallTimer = 0, frequencyTimer = 0;

	public RecurringEventScheduler(AbstractObject obj) {
		super(obj, "RecurringEventScheduler");
		events = new ArrayList<AbstractEvent>();
	}

	@Override
	protected void run() {
		for (int i = 0; i < events.size(); i++) {
			events.get(i).run();
		}
	}

	/**
	 * Schedules an event ( the {@code event} parameter Event), to play every
	 * {@code frequency} seconds, for {@code times} times .
	 * <p>
	 * You can choose to run another event ( the {@code endEvent} parameter
	 * Event) after finishing the number of times assigned in {@code times}.
	 * 
	 * @param event
	 *            An event that will run at a certain frequency
	 * @param frequency
	 *            How often (in seconds) the action will happen
	 * @param times
	 *            The number of times the event will run
	 * @param endEvent
	 *            The event to run after you ran the initial event for the
	 *            specified amount of times
	 * @param truTime
	 *            Whether or not to use real time or game time ( the one
	 *            multiplied by timeScale)
	 */
	public void addEvent(final AbstractEvent event, final float frequency,
			final int times, final AbstractEvent endEvent, final boolean truTime) {
		events.add(new AbstractEvent() {

			private boolean trueTime = truTime;
			private float freq = frequency, end = times * frequency + 0.05f,
					freqTimer = 0, endTimer = 0;
			private AbstractEvent eventToRun = event;

			@Override
			public synchronized void run() {
				if (trueTime) {
					freqTimer += rawDelta;
					endTimer += rawDelta;
				} else {
					freqTimer += delta;
					endTimer += delta;
				}

				// if end is -1, we want it to run all the time
				if (end != -1 && endTimer - end >= 0 && times != -1) {
					if (endEvent != null) {
						endEvent.run();
					}
					events.remove(events.indexOf(this));
				}
				if (freqTimer > freq) {
					eventToRun.run();
					freqTimer -= freq;
				}

			}
		});
	}

	public ArrayList<AbstractEvent> getEvents() {
		return events;
	}

	public void setEvents(ArrayList<AbstractEvent> events) {
		this.events = events;
	}

}
