package claudiu.framework;

import java.util.ArrayList;

import android.util.Log;


/** ReactToPosition is a behavior which helps with getting relevant 
 * trigonometry results between the object this behavior is applied to and 
 * {@link #focusedPoint}, which tailors to dynamically-changing points, without
 * having to change anything afterwards.
 * <p>
 * <b>In order for this behavior to work</b>, you need to set the focusedPoint, you
 * can do that in {@link #setFocusedPoint(Point)}.
 * <p>
 * The usefull methods that you have access to are : {@link #getAngle() }, 
 * which gets the angle between us and the focused point, {@link #getCos()} and
 *  {@link #getSin()}, that gets the sine and cosine between those two points,
 *  as well as {@link #getDistance()}, which gets the distance between those 2 
 *  objects.
 * <p>
 * Those functions can be used by adding events to {@link #events} that will be 
 * invoked every frame. You can add a new event in {@link #addEvent(AbstractEvent)}.
 *  One example for using this is having an Enemy object follow the Player, by 
 *  reacting to the Player's position, moving towards him.
 * 
 * 
 * @author Claudiu Bele
 * @see {@link Point}
 * @see {@link #setFocusedPoint(Point)}
 *
 */
public class ReactToPosition extends AbstractBehavior{
	
	
//	/**
//	 *  Variable that is updated every frame, in {@link #run()}, that 
//	 *  represents the angle between our object and the focused point.
//	 *  <p>
//	 *  It is changed every frame because {@link #getCos()} and 
//	 *  {@link #getSin()} rely on it.
//	 */
//	private float angle = 0;
	
	/**
	 *  The point which we focus and we use in getting trigonometry data.
	 *  <p>
	 *  Must be set in {@link #setFocusedPoint(Point)}.
	 */
	private Point focusedPoint;
	
	/** List of events that we will invoke every frame update.
	 * <p>
	 * You can add a new event in {@link #addEvent(AbstractEvent)}.
	 * 
	 */
	private ArrayList<AbstractEvent> events;

	public ReactToPosition(AbstractObject obj) {
		super(obj,"ReactToPosition");
		this.events = new ArrayList<AbstractEvent>();
		this.focusedPoint = new Point();
	}
	
	
	@Override
	protected void run() {
	
		// have to update angle in run method, because we use this angle
		// in getSin and getCos methods, which rely on angle.
		for( int i=0 ;i<events.size();i++){
			events.get(i).run();
		}
			
	}

	public Point getTrajectory(){
		return new Point(
				(focusedPoint.x - object.position.x)/ getDistance() ,
				(focusedPoint.y - object.position.y)/ getDistance() 
				);
	}
	
	public float getDistance(){
		return (float) Math.sqrt(
				(object.getPosition().x - focusedPoint.x) *
				(object.getPosition().x - focusedPoint.x) +
				(object.getPosition().y - focusedPoint.y) *
				(object.getPosition().y - focusedPoint.y));
	}
	public float getAngle(){
		return (float) Math.atan2
				(
				object.getPosition().y - focusedPoint.y,
				object.getPosition().x - focusedPoint.x
				);
	}
	public float getSin(){
		return (float) Math.sin(getAngle());
	}
	public float getCos(){
		return (float) Math.cos(getAngle());
	}
	public void addEvent(AbstractEvent event){
		this.events.add(event);
	}


	public Point getFocusedPoint() {
		return focusedPoint;
	}
	public void setFocusedPoint(Point focusedPosition) {
		this.focusedPoint = focusedPosition;
	}


	
}
