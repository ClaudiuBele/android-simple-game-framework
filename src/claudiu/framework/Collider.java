package claudiu.framework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import android.graphics.Rect;
import android.util.Log;

public class Collider extends AbstractBehavior{
	
	private HashMap<String,AbstractEvent> events;
	
	public ArrayList<ColliderRect> collideAreas;
	
	private Collider enemyCollider;
	
	
	
	
	
	
	public Collider(AbstractObject obj) {
		
		super(obj, "Collider");
		collideAreas = new ArrayList<ColliderRect>();
		events = new HashMap<String, AbstractEvent>();
		
	}
	/**
	 * Updates the Rectangles position, to be based on the position of the object
	 */
	@Override
	protected void runAtStartOfUpdate() {
		super.runAtStartOfUpdate();
		
		for(int i=0;i<collideAreas.size();i++){
			
			collideAreas.get(i).rectangle.set(
				(int)object.position.x + collideAreas.get(i).offsetX,
				(int)object.position.y + collideAreas.get(i).offsetY,
				(int)object.position.x + collideAreas.get(i).offsetX + collideAreas.get(i).width,
				(int)object.position.y + collideAreas.get(i).offsetY	+ collideAreas.get(i).height);

		}
	}
	@Override
	protected void run() {
			
			// getting all object types, organised in hashmaps of names and Objects
			for(Entry<String, HashMap<String, AbstractObject>> objectEntry : view.objectsMap.entrySet()){
				
				
				// our events don't contain the type as a key, so we don't react to them.
				if(!events.containsKey(objectEntry.getKey())){
					continue;
				}
				
				// getting all objects of a certain type
				for(Entry<String, AbstractObject> entry : objectEntry.getValue().entrySet()){	
				
					// check that the objects have colliders
					if(	entry.getValue().getBehavior("Collider") != null ){
						
						enemyCollider = (Collider) entry.getValue().getBehavior("Collider");
						if(doCollidersIntersect(this, enemyCollider)){
							events.get(objectEntry.getKey()).run();
							events.get(objectEntry.getKey()).run(entry.getValue());
						}
					}
				}
			}
			
		}		
	
	/** Checks whether or not 2 Rectangles intersect , called in {@link Collider#doCollidersIntersect(Collider, Collider)}
	 * 
	 * @param r1 The first Rectangle
	 * @param r2 The second Rectangle
	 * @return
	 */
	public static boolean rectanglesIntersect(Rect r1, Rect r2){
		if(
				r2.contains(r1.left, r1.top) || 
				r2.contains(r1.left, r1.bottom) ||
				r2.contains(r1.right, r1.top) ||
				r2.contains(r1.right, r1.bottom) ||
				
				r1.contains(r2.left, r2.top) ||
				r1.contains(r2.left, r2.bottom) ||
				r1.contains(r2.right, r2.top) ||
				r1.contains(r2.right, r2.bottom)||
				
				r1.contains(r2) ||
				r2.contains(r1) 

			)
		{
			return true;
		}
		return false;
	}
	
	/** Checks whether or not 2 Colliders intersect , called in {@link Collider#run()}
	 * <p>
	 * Goes over both Collider's collideAreas list, checking every area from one collider against every area from the other collider, by using {@link Collider#rectanglesIntersect(Rect, Rect)}
	 * 
	 * @param c1 The first Collider
	 * @param c2 The second Collider
	 * @return
	 */
	public static boolean doCollidersIntersect(Collider c1, Collider c2){
		for(int i=0;i<c1.collideAreas.size();i++){
			for(int j=0 ; j < c2.collideAreas.size() ; j++){
				if(rectanglesIntersect(c1.collideAreas.get(i).rectangle, c2.collideAreas.get(j).rectangle)){
					return true;
				}
			}
		}
		return false;
	}
	
	public class ColliderRect{
		
		protected int width,height,offsetX, offsetY;
		protected Rect rectangle;
		public ColliderRect(int width, int height, int offsetX, int offsetY){
			this.width = width;
			this.height =  height;
			this.offsetX = offsetX;
			this.offsetY = offsetY;
			
			this.rectangle = new Rect(
					(int)object.position.x-offsetX,
					(int)object.position.y-offsetY,
					(int)object.position.x+width+offsetX,
					(int)object.position.y+height+offsetY);
		}
	}
	
	public void addEvent(String key, AbstractEvent event){
		events.put(key, event);
	}
	public void addCollideArea(int size){
		addCollideArea(size, size);
	}
	public void addCollideArea(int width, int height){
		addCollideArea(width, height, -width/2, -height/2);
	}
	public void addCollideArea(int width, int height, int offsetX, int offsetY){
		collideAreas.add(new ColliderRect(width, height, offsetX, offsetY));
	}
}
