package claudiu.framework;

import java.util.ArrayList;

import android.util.Log;

/** ReactToAccelerometer is a behavior that we can attach to an AbstractObject,
 *  in which we can react to the values of the 3 axis of the accelerometer. The
 *   values are assigned in {@link #runAtStartOfUpdate()}, 
 *   by getting the values from {@link Game#acceleration}.
 *   <p>
 *   All the values range between -1 and 1, and the way the values are assigned
 *   by applying a low pass filter of 0.2f ( probably too much ), to the raw 
 *   acceleration divided by 10. We do this in {@link AbstractView#onSensorChanged(android.hardware.SensorEvent)}
 *   <p>
 *   Relevant values can be obtained from {@link #getX()}, {@link #getY()}, {@link #getZ()}, 
 *   or you can have all variables in a {@link Point3D} object, {@link #getAcceleration()}
 *   <p>
 *   If you want the data to have a highpass Filter or low pass filter, you can change {@link #filterRate} with
 *   {@link #setFilterRate(float) }, which will be clamped between 0 and 1, 0 meaning that we take only old data,
 *   1 that we take only new data, and 0.5 we take half and half.
 * 
 * @author Claudiu
 *
 */
public class ReactToAccelerometer extends AbstractBehavior {
	
	/**
	 * The x ,y, and z of the accelerometer.
	 */
	private float x, y, z;

	/**
	 * Point with 3 coordinates
	 */
	private Point3D acceleration;

	/**
	 * Accelerations that we have to get to ( needed because of the filtering)
	 */
	private Point3D accelerationGoalValues;
	
	/**
	 * List in which we hold all the events that will be called each frame
	 * update.
	 */
	private ArrayList<AbstractEvent> events;
	
	/** How much out of the previous Accelerometer data will make up the new
	 * accerometer data. It is used in {@link ReactToAccelerometer#feedData(Point3D)}.
	 * <p>
	 * A value of 0, means we don't take into account the old Accelerometer data
	 * <p>
	 * A value of 1, means we take into account only the old Accelerometer data.
	 * <p>
	 * In constructor, it is assigned to 0.
	 * 
	 * 
	 */
	private float filterRate;

	public ReactToAccelerometer(AbstractObject obj) {
		super(obj, "ReactToAccelerometer");
		this.acceleration = new Point3D();
		this.accelerationGoalValues = new Point3D();
		this.filterRate = 0f;
		x = y = z = 0;
		this.events = new ArrayList<AbstractEvent>();
	}
	/** Updates the accelerometer data with fresh new data from the sensor.
	 * It takes {@link ReactToAccelerometer#filterRate} of old data, and 
	 * (1- filterRate ) out of the new data.
	 * @see #accelerationGoalValues
	 * @see #acceleration
	 * @see AbstractBehavior#update()
	 * @see #feedData(Point3D)
	 */
	@Override
	protected void runAtStartOfUpdate() {
		super.runAtStartOfUpdate();
		
		// Update X data
		this.acceleration.setX(
				filterRate * this.acceleration.getX() +
				(1 - filterRate) * accelerationGoalValues.getX()) ;
		this.acceleration.setX(Math.max(Math.min(1f, this.acceleration.getX()),-1f));
		this.x = this.acceleration.getX();
		
		
		// Update Y data
		this.acceleration.setY(
				filterRate * this.acceleration.getY() +
				(1 - filterRate) * accelerationGoalValues.getY()) ;
		this.acceleration.setY(Math.max(Math.min(1f, this.acceleration.getY()),-1f));
		this.y = this.acceleration.getY();
		
		
		// Update Z data
		this.acceleration.setZ(
				filterRate * this.acceleration.getZ() + 
				(1 - filterRate) * accelerationGoalValues.getZ()) ;
		this.acceleration.setZ(Math.max(Math.min(1f, this.acceleration.getZ()),-1f));
		this.z = this.acceleration.getZ();
	}
	@Override
	protected void run() {
		for (int i = 0; i < events.size(); i++) {
			events.get(i).run();
			events.get(i).run(
					new Float[] { Float.valueOf(acceleration.getX()),
							Float.valueOf(acceleration.getY()),
							Float.valueOf(acceleration.getZ()) });
			events.get(i).run(acceleration);
		}
	}

	
	
	/**
	 * Adds an event to the {@link ReactToAccelerometer#events }, which will be
	 * iterated over and all event will be ran, in
	 * {@link ReactToAccelerometer#run()}
	 * 
	 * @param event the event that we want to add
	 */
	public void addEvent(AbstractEvent event) {
		events.add(event);
	}

	/** Updates our goal acceleration values to new ones.
	 * <p>
	 * Called on all the objects in the currentView on a sensor change in 
	 * {@link AbstractView#onSensorChanged(android.hardware.SensorEvent)}.
	 * 
	 * @see AbstractView#onSensorChanged(android.hardware.SensorEvent)
	 * @see ReactToAccelerometer#filterRate
	 * @param newAcceleration The new unmodified data taken from the sensor.
	 * 
	 */
	public void feedData(Point3D newAcceleration) {
			this.accelerationGoalValues = newAcceleration;
	}
	
	
	
	
	
	// Setters and getters
	public void setFilterRate(float filterRate) {
		this.filterRate = Math.min(Math.max(filterRate, 0f),1f);
		Log.v("Game","Set the filter rate to "+this.filterRate);
	}
	public float getFilterRate() {
		return filterRate;
	}
	public float getX() {
		return x;
	}
	public float getY() {
		return y;
	}
	public float getZ() {
		return z;
	}
	public Point3D getAcceleration() {
		return acceleration;
	}

	

}
