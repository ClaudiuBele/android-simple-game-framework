package claudiu.framework;

import android.view.MotionEvent;

/**
 * Behavior that handles an object's responses to the user touching the screen,
 * and other input-related events
 * <p>
 * In order to get the input's X and Y you have to get them from Game class,
 * {@link Game#input} for an input point, or {@link Game#inputX} and 
 * {@link Game#inputY} for individual values.
 * <p>
 * for {@link MotionEvent#ACTION_DOWN} use {@link ReactToTouch#downEvent}
 * <p>
 * for {@link MotionEvent#ACTION_UP} use {@link ReactToTouch#upEvent}
 * <p>
 * for {@link MotionEvent#ACTION_MOVE} use {@link ReactToTouch#moveEvent}
 * 
 * @author Claudiu Bele
 * @see Game#input
 * @see Game#inputX
 * @see Game#inputY
 */
public class ReactToTouch extends AbstractBehavior {

	/**
	 * Event that is triggered when you move the finger on the screen
	 */
	private AbstractEvent moveEvent;
	/**
	 * Event that is triggered when you put your finger on the screen
	 */
	private AbstractEvent downEvent;
	/**
	 * Event that is triggered when you take your finger away from the screen
	 */
	private AbstractEvent upEvent;
	
	/**
	 * The offset X and Y 
	 */
	private float offsetX, offsetY;
	
	/**
	 *  The width and height of the clickable area
	 */
	private float width, height;

	public ReactToTouch(AbstractObject obj) {
		super(obj, "ReactToTouch");
		moveEvent = new AbstractEvent() {
			@Override
			public void run() {}
		};
		downEvent = new AbstractEvent() {
			@Override
			public void run() {}
		};
		upEvent = new AbstractEvent() {
			@Override
			public void run() {}
		};
		setSize(50);
		setPointIsCenter();
	}
	public boolean doesPointClick(Point touch){
		if(
				touch.x >= object.position.x + offsetX 			
			&& 	touch.x <= object.position.x + offsetX + width	
			&& 	touch.y >= object.position.y + offsetY			
			&& 	touch.y <= object.position.y + offsetY + height	
		){
			return true;
		}
		return false;
	}

	
	/** Sets the image to be drawn having the middle the object's position
	 * PRECONDITION : Set Size of Image
	 * 
	 */
	public void setPointIsCenter(){
		this.offsetX = - this.width / 2;
		this.offsetY = - this.height/ 2;
	}
	@Override
	protected void run() {
	}

	
	
	
	
	public void setSize(float size){
		this.width  = size;
		this.height = size;
	}
	public void setSize(float width, float height){
		this.width 	= width;
		this.height = height;
	}
	public void setOffset(float x, float y){
		this.offsetX = x;
		this.offsetY = y;
	}
	
	
	public AbstractEvent getMoveEvent() {
		return moveEvent;
	}
	public void setMoveEvent(AbstractEvent moveEvent) {
		this.moveEvent = moveEvent;
	}
	public AbstractEvent getDownEvent() {
		return downEvent;
	}
	public void setDownEvent(AbstractEvent downEvent) {
		this.downEvent = downEvent;
	}
	public AbstractEvent getUpEvent() {
		return upEvent;
	}
	public void setUpEvent(AbstractEvent upEvent) {
		this.upEvent = upEvent;
	}

}
